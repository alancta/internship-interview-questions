# Internship Interview Questions
*Please include explanation along with your answers.*

1. Please describe yourself using JSON
```
{
  "name": "Alan Chuan",
  "dateOfBirth": "03/08/1999",
  "age": 20,
  "countryOfOrigin": "Malaysia",
  "currentUniversity": "University of Wisconsin-Madison",
  "interests": [
    "singing",
    "coding",
    "cooking",
    "business management",
    "entrepreneurship",
    "philosophy"
  ],
  "fears": ["heights", "cockroaches"],
  "spokenLanguages": ["English", "Mandarin", "Malay"]
}
```
The json above contains String fields with my personal details, which are represented by Strings ("name", "age" etc. ), an int for my age (20), and an array of strings to contain a list of items.

2. Tell us about a newer (less than five years old) web technology you like and why?

I'm a fan of Vue(last year was its stable release) for building smaller scale web apps. I started to pick up Vue just last week and I realized that the learning curve was not too steep, their tutorials were pretty user-friendly and easy to follow. As long as someone has knowledge of HTML, Vue is relatively easier to learn as it uses an HTML-based template syntax that enables 2-way binding of the elements rendered to the DOM with the Vue instance data.

3. In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that?

Instead of utilizing a fixed array with a maximum size that needs to be set upon initialization, we can utilize an ArrayList which is a resizable array which offers the freedom of setting the capacity upon initialization OR allow it to be initialized with a default capacity, and expands as more items are added over time. In an ArrayList, the resizing is done automatically for us whenever an ArrayList is full.

4. Explain this block of code in Big-O notation
```
    void sampleCode(int arr[], int size)
    {   
      *//This outer loop executes N times, where N = size, O(N)*
        for (int i = 0; i < size; i++)  
        {
          *//This inner loop also executes N times, where N = size, O(N)*
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
```
The outer loop executes N times. For *every* time the outer loop executes, the inner loop also executes N times.
This means the Big-O notation is O(N*N) = O(N^2).  

5. One of our developers built a page to display market information for over 500 cryptocurrencies in a single page. After several days, our support team started receiving complaints from our users that they are not able to view the website and it takes too long to load. What do you think causes this issue and suggest at least 3 ideas to improve the situation.

The main contributing factor is probably amount of data that has to be loaded for a list of 500 cryptocurrencies. To tackle the issue, we can look at:-

1) Minifying html/css/js
By minifying the js, css, and html files, extra spaces will be removed which in turns minimizes code and file size. By "minification", less data will need to be processed the same functionality can be achieved while improving the load speed.

2) Utilizing a content delivery network(CDN)
As CoinGecko has a global reach and is accessed by users across the globe, a CDN will enable information to be delivered to the user via the server closest to their location, speeding up the site's load time. On top of that, it will provide some protection from a surge in traffic. There are some CDN services which automatically provide minification, which is an added advantage.

3) Bundle Javascript modules
Instead of listing javascript src paths one by one, we can use bundle modules together by concatenating it into one big file, so to reduce the number of requests made.

As a last resort, if the problem still persists and is faced by a significant amount of users, perhaps we can set a lower limit for the maximum number of pages being displayed per page, because a quick loading time is key in preventing bounce rates.


**Thank you for your time! :)**
